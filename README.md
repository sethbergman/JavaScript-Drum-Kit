# JavaScript Drum Kit

##### Play the drums on your keyboard

[![Build Status](https://travis-ci.org/sethbergman/JavaScript-Drum-Kit.svg?branch=master)](https://travis-ci.org/sethbergman/JavaScript-Drum-Kit)

[![Greenkeeper badge](https://badges.greenkeeper.io/sethbergman/JavaScript-Drum-Kit.svg)](https://greenkeeper.io/)

---

### Install dependencies

```sh
npm i # or use yarn
yarn
```

### Start the application

```sh
npm start # or
yarn start
```

### Jam out :smiley:

Navigate to http://localhost:5000 and have fun!

#### More documentation coming soon :octocat:
